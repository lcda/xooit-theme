<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="{S_CONTENT_DIRECTION}">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
  {META}
  {NAV_LINKS}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <script language="javascript" src="http://img.xooimage.com/files8/s/c/script-906f2.js">
  </script>

  <style type="text/css">

    /*
      flower_of_evil Theme for phpBB version 2+
      Created by larme d'ange
      http://lda-creation.conceptbb.com

      NOTE: These CSS definitions are stored within the main page body so that you can use the phpBB2
      theme administration centre. When you have finalised your style you could cut the final CSS code
      and place it in an external file, deleting this section to save bandwidth.
    */

    /* General page style. The scroll bar colours only visible in IE5.5+ */
    body {

      background-image: url(http://img.xooimage.com/files6/e/b/6/back-test-gif-2-2e591a.gif);
      background-attachment: fixed;
      background-repeat: no-repeat;
      background-position: top left;
      background-color: {T_BODY_BGCOLOR};
      scrollbar-face-color: #111111;
      scrollbar-highlight-color: #666666;
      scrollbar-shadow-color: #444444;
      scrollbar-3dlight-color: #333333;
      scrollbar-arrow-color:  #777777;
      scrollbar-track-color: #202020;
      scrollbar-darkshadow-color: #222222;
    }

    /* General font families for common tags */
    font,th,td,p { font-family: {T_FONTFACE1} }
    a:link,a:active,a:visited { color : {T_BODY_LINK}; }
    a:hover    { text-decoration: none; color : {T_BODY_HLINK}; }
    hr  { height: 0px; border: solid {T_TR_COLOR3} 0px; border-top-width: 1px;}

    /* This is the border line & background colour round the entire page */
    .bodyline  { background-color: ""; border: 0px {T_TH_COLOR1} solid; }

    /* This is the outline round the main forum tables */
    .forumline  { background-color: {T_TH_COLOR1}; border: 2px {T_TH_COLOR2} solid; }

    /* Main table cell colours and backgrounds */
    td.row1  { background-color: {T_TR_COLOR1}; }
    td.row2  { background-color: {T_TR_COLOR2}; }
    td.row3  { background-color: {T_TR_COLOR3}; }

    /*
      This is for the table cell above the Topics, Post & Last posts on the index.php page
      By default this is the fading out gradiated silver background.
      However, you could replace this with a bitmap specific for each forum
    */
    td.rowpic {
        background-color: {T_TR_COLOR3};
        background-image: url(http://img.xooimage.com/files2/2/5/1/back_catd-1-2e3ee2.jpg);
        background-repeat: repeat-y;
    }

    /* Header cells - the blue and silver gradient backgrounds */
    th  {
      color: {T_FONTCOLOR3}; font-size: {T_FONTSIZE2}px; font-weight : bold;
      background-color: {T_BODY_LINK}; height: 25px;
      background-image: url(http://img.xooimage.com/files6/b/a/back_title-71eb2.jpg);
    }

    td.cat,td.catHead,td.catSides,td.catLeft,td.catRight,td.catBottom {
          background-image: url(http://img.xooimage.com/files9/6/0/8/back_catd-1-2e3ee7.jpg);
          background-color:{T_TR_COLOR3}; border: {T_TH_COLOR3}; border-style: solid; height: 28px;
    }

    /*
      Setting additional nice inner borders for the main table cells.
      The names indicate which sides the border will be on.
      Don't worry if you don't understand this, just ignore it :-)
    */
    td.cat,td.catHead,td.catBottom {
      height: 29px;
      border-width: 0px 0px 0px 0px;
    }
    th.thHead,th.thSides,th.thTop,th.thLeft,th.thRight,th.thBottom,th.thCornerL,th.thCornerR {
      font-weight: bold; border: {T_TD_COLOR2}; border-style: solid; height: 25px;
    }
    td.row3Right,td.spaceRow {
      background-color: {T_TR_COLOR3}; border: {T_TH_COLOR3}; border-style: solid;
    }

    th.thHead,td.catHead { font-size: {T_FONTSIZE3}px; border-width: 1px 1px 0px 1px; }
    th.thSides,td.catSides,td.spaceRow   { border-width: 0px 1px 0px 1px; }
    th.thRight,td.catRight,td.row3Right   { border-width: 0px 1px 0px 0px; }
    th.thLeft,td.catLeft    { border-width: 0px 0px 0px 1px; }
    th.thBottom,td.catBottom  { border-width: 0px 1px 1px 1px; }
    th.thTop   { border-width: 1px 0px 0px 0px; }
    th.thCornerL { border-width: 1px 0px 0px 1px; }
    th.thCornerR { border-width: 1px 1px 0px 0px; }

    /* The largest text used in the index page title and toptic title etc. */
    .maintitle  {
      font-weight: bold; font-size: 18px; font-family: "{T_FONTFACE2}",{T_FONTFACE1};
      text-decoration: none; line-height : 120%; color : {T_BODY_TEXT};
    }

    /* General text */
    .gen { font-size : {T_FONTSIZE3}px; }
    .genmed { font-size : {T_FONTSIZE2}px; }
    .gensmall { font-size : {T_FONTSIZE1}px; }
    .gen,.genmed,.gensmall { color : {T_BODY_TEXT}; }
    a.gen,a.genmed,a.gensmall { color: {T_BODY_LINK}; text-decoration: none; }
    a.gen:hover,a.genmed:hover,a.gensmall:hover  { color: {T_BODY_HLINK}; text-decoration: none; }

    /* The register, login, search etc links at the top of the page */
    .mainmenu    { font-size : {T_FONTSIZE2}px; color : {T_BODY_TEXT} }
    a.mainmenu    { text-decoration: none; color : {T_BODY_LINK};  }
    a.mainmenu:hover{ text-decoration: none; color : {T_BODY_HLINK}; }

    /* Forum category titles */
    .cattitle    { font-weight: bold; font-size: {T_FONTSIZE3}px ; letter-spacing: 1px; color : {T_BODY_LINK}}
    a.cattitle    { text-decoration: none; color : {T_BODY_LINK}; }
    a.cattitle:hover{ text-decoration: none; }

    /* Forum title: Text and link to the forums used in: index.php */
    .forumlink    { font-weight: bold; font-size: {T_FONTSIZE3}px; color : {T_BODY_LINK}; }
    a.forumlink   { text-decoration: none; color : {T_BODY_LINK}; }
    a.forumlink:hover{ text-decoration: none; color : {T_BODY_HLINK}; }

    /* Used for the navigation text, (Page 1,2,3 etc) and the navigation bar when in a forum */
    .nav      { font-weight: bold; font-size: {T_FONTSIZE2}px; color : {T_BODY_TEXT};}
    a.nav      { text-decoration: none; color : {T_BODY_LINK}; }
    a.nav:hover    { text-decoration: none; }

    /* titles for the topics: could specify viewed link colour too */
    .topictitle,h1,h2  { font-weight: bold; font-size: {T_FONTSIZE2}px; color : {T_BODY_TEXT}; }
    a.topictitle:link   { text-decoration: none; color : {T_BODY_LINK}; }
    a.topictitle:visited { text-decoration: none; color : {T_BODY_VLINK}; }
    a.topictitle:hover  { text-decoration: none; color : {T_BODY_HLINK}; }

    /* Name of poster in viewmsg.php and viewtopic.php and other places */
    .name      { font-size : {T_FONTSIZE2}px; color : {T_BODY_TEXT};}

    /* Location, number of posts, post date etc */
    .postdetails    { font-size : {T_FONTSIZE1}px; color : {T_BODY_TEXT}; }

    /* The content of the posts (body of text) */
    .postbody { font-size : {T_FONTSIZE3}px; line-height: 18px}
    a.postlink:link  { text-decoration: none; color : {T_BODY_LINK} }
    a.postlink:visited { text-decoration: none; color : {T_BODY_VLINK}; }
    a.postlink:hover { text-decoration: none; color : {T_BODY_HLINK}}

    /* Quote & Code blocks */
    .code {
      font-family: {T_FONTFACE3}; font-size: {T_FONTSIZE2}px; color: {T_FONTCOLOR2};
      background-color: {T_TD_COLOR1}; border: {T_TH_COLOR1}; border-style: solid;
      border-left-width: 1px; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px
    }

    .quote {
      font-family: {T_FONTFACE1}; font-size: {T_FONTSIZE2}px; color: {T_FONTCOLOR1}; line-height: 125%;
      background-color: {T_TD_COLOR1}; border: {T_TH_COLOR1}; border-style: solid;
      border-left-width: 1px; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px
    }

    /* Copyright and bottom info */
    .copyright    { font-size: {T_FONTSIZE1}px; font-family: {T_FONTFACE1}; color: {T_FONTCOLOR1}; letter-spacing: -1px;}
    a.copyright    { color: {T_FONTCOLOR3}; text-decoration: none;}
    a.copyright:hover { color: {T_FONTCOLOR2}; text-decoration: none;}

    /* Form elements */
    input,textarea, select {
        border-left-width: 1px; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px;
      color : {T_FONTCOLOR3};
      font: normal {T_FONTSIZE2}px {T_FONTFACE1};
      border-color : {T_FONTCOLOR1};
    }

    /* The text input fields background colour */
    input.post, textarea.post, select {
      background-color : {T_TH_COLOR3};
    }

    input { text-indent : 2px;
          background-color : {T_TD_COLOR1}; }

    /* The buttons used for bbCode styling in message post */
    input.button {
      background-color : {T_TR_COLOR2};
      color : {T_BODY_TEXT};
      font-size: {T_FONTSIZE2}px; font-family: {T_FONTFACE1};
    }

    /* The main submit button option */
    input.mainoption {
      background-color : {T_TD_COLOR1};
      font-weight : bold;
    }

    /* None-bold submit button */
    input.liteoption {
      background-color : {T_TD_COLOR1};
      font-weight : normal;
    }

    /* This is the line in the posting page which shows the rollover
      help line. This is actually a text box, but if set to be the same
      colour as the background no one will know ;)
    */
    .helpline { background-color: {T_TR_COLOR2}; border-style: none; }

    /* Import the fancy styles for IE only (NS4.x doesn't use the @import function) */
    @import url("http://img.xooimage.com/files10/f/o/formie-30d7c.css");

  </style>

  <link href='https://fonts.googleapis.com/css?family=Marck+Script' rel='stylesheet' type='text/css'>
  <style type="text/css">
/*==========================================================================*/
/*==========================================================================*/



body{
font-size: 16pt;
}
a{
color: #777;
font-weight: bold;
text-decoration: none;
transition-property: color;
transition-duration: 0.15s;
}
a:hover{
color: #288AD7;
}
html{
margin: 0 !important;
position: relative;
}
body{
padding: 0 !important;
background-attachment: scroll;
background-color: #000;
background-image: url(http://lcda.gitlab.io/xooit-files/img/bg_cloth.jpg);
background-repeat: repeat;
}
body>div.bglayer{
position: absolute;
left:0; right:0; top:0; bottom:0;
width: 100%;
height:100%;
z-index: -1;
background-repeat: no-repeat;
background-size: 100% auto;
}
#bglayertop{
background-image: url(http://lcda.gitlab.io/xooit-files/img/bglayer_top.png);
background-position: top;
}
#bglayerbot{
background-image: url(http://lcda.gitlab.io/xooit-files/img/bglayer_bot.png);
background-position: bottom;
}
header{
display: table-cell;
top: 0;
width: 100vw;
text-align: center;
height: 35vw;
vertical-align: middle;
}
#navbar{
text-align: center;
}
#navbar>a, a.linkbutton{
display: inline-block;
text-align: center;
letter-spacing: -0.05em;
vertical-align: middle;
font-size: 25px;
font-family: 'Marck Script', cursive;
font-weight: normal;
color: #FFF;
transition-property: text-shadow;
transition-duration: 0.5s;
border: none !important;
text-decoration: none;
}
#navbar>a:hover, a.linkbutton:hover{
text-shadow: 0 0 15px #b6e4ff;
}
a.linkbutton{
display: inline-block;
width: 105px;
height: 50px;
line-height: 50px; background-image: url(http://lcda.gitlab.io/xooit-files/img/button.png);
background-size: 100% 50px;
}
#navbar>a{
margin-left: 5px;
margin-right: 5px;
}
#contenttable{
margin: 10vw auto 10vw auto;
border-collapse: collapse;
}
#contenttable>tbody>tr>td,
#contenttable>tr>td{
padding: 0;
}
#contenttable .content_tl,
#contenttable .content_tr,
#contenttable .content_bl,
#contenttable .content_br{
background-color: none;
background-image: url(http://lcda.gitlab.io/xooit-files/img/corners.png);
width: 64px; height: 64px;
}
#contenttable .content_tl{background-position: left 0px;}
#contenttable .content_tr{background-position: right 0px;}
#contenttable .content_bl{background-position: left -64px;}
#contenttable .content_br{background-position: right -64px;}
#contenttable .content_t,
#contenttable .content_b{
background-image: url(http://lcda.gitlab.io/xooit-files/img/cont_hrz.png);
width: auto; height: 64px;
}
#contenttable .content_t{background-position: top;}
#contenttable .content_b{background-position: bottom;}
#contenttable .content_l,
#contenttable .content_r{
background-image: url(http://lcda.gitlab.io/xooit-files/img/cont_vrt.png);
width: 64px; height: auto;
}
#contenttable .content_l{background-position: left;}
#contenttable .content_r{background-position: right;}
#contenttable .content_fill{
background-color: rgba(0,0,0,0.75);
}
#contenttable .redbanner_l,
#contenttable .redbanner_r{
background-image: url(http://lcda.gitlab.io/xooit-files/img/corners.png);
width: 64px; height: 118px;
}
#contenttable .redbanner_l{background-position: left -128px;}
#contenttable .redbanner_r{background-position: right -128px;}
#contenttable .redbanner_fill{
background-image: url(http://lcda.gitlab.io/xooit-files/img/redbanner_fill.png);
width: auto; height: 100px;
padding-bottom: 18px;
}
#content{
max-width: 1000px;
min-height: 500px;
}
#content>br{
display: none;
}
#content table{
border: none;
border-collapse: collapse;
font-size: 0.6em;
}
#content table.forumline{
margin-top: 50px;
border: 0;
}
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child>td:first-child,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child>th:first-child{
background: none;
background-image: url(http://lcda.gitlab.io/xooit-files/img/eye.png);
background-repeat: no-repeat;
height: 64px;
border: none;
}
#content table.forumline{
background: none;
background-color: rgba(0,0,0,0.4);
border-radius: 25px 25px 0 0;
border-bottom: 1px solid rgba(0, 140, 255, 0.05);
}
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child th,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child .cattitle,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child td,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child td span,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr:first-child .nav,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr .thLeft,
#content table.forumline:not(#chatMsgContainer_table)>tbody>tr .thRight, th.thSides{
background: none;
border: none;
color: #FFF;
font-weight: normal;
text-align: center;
font-family: 'Marck Script', cursive;
font-size: 25px;
letter-spacing: -0.05em;
}
td.row1, td.row3, th.thLeft, th.thRight, td.spaceRow{
border: none;
background: none;
}
td.row1 a.forumlink, a.topictitle, .thSides{
padding-left: 10px;
font-family: 'Marck Script', cursive;
font-size: 1.5em;
transition-property: color;
transition-duration: 0.2s;
}
.cattitle{
font-family: 'Marck Script', cursive;
font-weight: normal;
font-size: 1.4em;
}
td.row2, td.row3Right{
border: none;
background-color: rgba(0, 0, 0, 0.5);
}
td.cat,td.catHead,td.catSides,td.catLeft,td.catRight,td.catBottom, td.rowpic,
#content table.forumline .thHead{
background: none;
border: none;
padding-left: 10px;
}
#maintitle{
display: block;
height: 80px;
color: #FFF;
text-shadow: 0 0 15px #66b4cf;
font-family: 'Marck Script', cursive;
font-weight: normal;
font-size: 50px;
text-align: center;
}

#link_message:not([newmsg="http://img.xooimage.com/files7/9/a/9/icon_mini_message-906ea.png"]){
text-shadow: 0 0 10px #000;
color: #288AD7 !important;
}





/*==========================================================================*/
/*==========================================================================*/
  </style>

  <script language="Javascript" type="text/javascript">
    if ( {PRIVATE_MESSAGE_NEW_FLAG} )
    {
      window.open('{U_PRIVATEMSGS_POPUP}', '_phpbbprivmsg', 'HEIGHT=225,resizable=yes,WIDTH=400');;
    }
  </script>
  <link rel="shortcut icon" href="http://lcda-nwn2.fr/favicon.ico"/>
</head>
<!-- ============================================================================================== -->
<body bgcolor="{T_BODY_BGCOLOR}" text="{T_BODY_TEXT}" link="{T_BODY_LINK}" vlink="{T_BODY_VLINK}">
  <div class="bglayer" id="bglayerbot"></div>
  <div class="bglayer" id="bglayertop"></div>

  <table id="contenttable">
    <tr>
      <td class="content_tl"></td>
      <td class="content_t"></td>
      <td class="content_tr"></td>
    </tr>
    <tr>
      <td class="content_l"></td>
      <td id="maintitle" class="content_fill">La Colère d 'Aurile</td>
      <td class="content_r"></td>
    </tr>
    <tr>
      <td class="redbanner_l"></td>
      <td class="redbanner_fill">
        <nav id="navbar">
          <a href="{U_PORTAL}" class="mainmenu">Portail</a>
          <a href="{U_INDEX}" class="mainmenu">Forum</a>
          <a href="https://wiki.lcda-nwn2.fr" target="_blank" class="mainmenu">Wiki</a>
          <a href="https://account.lcda-nwn2.fr" target="_blank" class="mainmenu">Compte nwn2</a>
          <br/>
          <!-- BEGIN switch_user_logged_in -->
            <a href="{U_SEARCH}" class="mainmenu">Rechercher</a>
            <a href="{U_MEMBERLIST}" class="mainmenu">Membres</a>
            <a href="{U_GROUP_CP}" class="mainmenu">Groupes</a>
            <a href="{U_PROFILE}" class="mainmenu">Profil</a>
            <a href="{U_PRIVATEMSGS}" class="mainmenu" id="link_message" newmsg="{PRIVMSG_IMG}">Messages</a>
            <a href="{U_LOGIN_LOGOUT}" class="mainmenu">Déconnexion</a>
          <!-- END switch_user_logged_in -->
          <!-- BEGIN switch_user_logged_out -->
            <a href="{U_SEARCH}" class="mainmenu">Rechercher</a>
            <a href="{U_REGISTER}" class="mainmenu">S'enregistrer</a>
            <a href="{U_LOGIN_LOGOUT}" class="mainmenu">Connexion</a>
          <!-- END switch_user_logged_out -->
        </nav>
      </td>
      <td class="redbanner_r"></td>
    </tr>
    <tr>
      <td class="content_l"></td>
      <td class="content_fill">
        <section id="content">
        <!-- BEGINCONTENT -->


