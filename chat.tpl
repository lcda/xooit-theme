<table class="forumline forum-table" border="0" cellspacing="1" width="100%">
 <tr>
  <th class="thTop" nowrap="nowrap" colspan="3">&nbsp;Chat&nbsp;</th>
 </tr>
 <tr class="bg2">
  <!-- Discord widget -->
  <td rowspan="4" style="padding-right: 10px"><iframe src="https://discordapp.com/widget?id=319773600905297922&theme=dark" width="250" height="320" allowtransparency="true" frameborder="0"></iframe></td>

  <td class="row3" style="padding:3px"<!-- IF CHAT_SHOW_CONNECTED --> colspan="3"<!-- ENDIF -->>
   <div id="chatTopic" class="genmed" style="font-weight:bold">
    <div id="chatArchiveLink" style="display:none;float:right">
     <a href="javascript:void(0);">[{L_CHAT_ARCHIVES}]</a>
    </div>
    <!-- IF CHAT_AUTH_MOD -->
    <div id="chatAdminLink" style="display:none;float:right">
     <a href="javascript:void(0);">[{L_CHAT_ADMIN}]</a>
    </div>
    <div id="chatAdminMenu" style="display:none;float:right">
     <div id="chatAdminMenuDiv">
      <a id="chatAdminMenuTopic" href="javascript:void(0);">[{L_CHAT_CHANGE_TOPIC}]</a>
      <a id="chatAdminMenuBanned" href="javascript:void(0);">[{L_CHAT_BANNED_USERS}]</a>
      <a id="chatAdminMenuClear" href="javascript:void(0);">[{L_CHAT_CLEAR_MSGS}]</a>
      <a id="chatAdminMenuClose" href="javascript:void(0);">[{L_CHAT_CLOSE_ADMIN}]</a>
     </div>
    </div>
    <div id="chatConnectLink" style="float:right">
     <a href="javascript:void(0);">[{L_CHAT_CONNECTION}]</a>
    </div>
    <div id="chatDisconnectLink" style="float:right">
     <a href="javascript:void(0);">[{L_CHAT_DISCONNECTION}]</a>
    </div>
    <div id="chatTopicContainer"></div>
    <!-- ENDIF -->
   </div>
  </td>
 </tr>
 <tr class="bg1" style="width:100%">
  <td class="row1" style="padding:0;width:100%">
   <div id="chatList" style="height:100px;overflow:auto;width:100%">
    <table id="chatMsgContainer_table" class="forumline" border="0" cellspacing="1" width="100%" style="border:0">
     <tbody id="chatMsgContainer">
     </tbody>
    </table>
   </div>
  </td>
  <!-- IF CHAT_SHOW_CONNECTED -->
  <td id="chatGripTd" class="row2" style="width:3px;cursor:w-resize">&nbsp;</td>
  <td class="row1" style="vertical-align:top">
   <div id="chatConnectedDiv" style="width:100%;height:150px;overflow:auto"></div>
  </td>
  <!-- ENDIF -->
 </tr>
 <tr class="bg2">
  <td class="row1"<!-- IF CHAT_SHOW_CONNECTED --> colspan="3"<!-- ENDIF -->>
  <img src="http://img.xooimage.com/files5/d/6/7/grip-115818.gif" width="11" height="11" alt="resize" style="cursor:n-resize;float:right;padding-top:10px" id="chatVGrip" title="{L_CHAT_RESIZE}" /><img src="http://img.xooimage.com/files10/5/e/a/window-115821.gif" style="cursor:pointer;padding-top:10px;padding-right:3px;width:11px;height:11px;float:right;" onclick="window.open('{U_CHAT}','_blank','menubar=no,toolbar=no,resizable=yes,scrollbars=yes');" title="{L_CHAT_OPEN_POPUP}" id="chatPopupButton" /><input id="chatButtonF" type="button" class="liteoption button2" style="background-position:center;background-repeat:no-repeat" title="{L_CHAT_HELP_SOUND}" value="   " /><input id="chatButtonG" type="button" class="liteoption button2" style="font-weight:bold" title="{L_CHAT_HELP_BOLD}" value=" G " /><input id="chatButtonI" type="button" class="liteoption button2" style="font-style:italic" title="{L_CHAT_HELP_ITALIC}" value=" I " /><input id="chatButtonC" type="button" class="liteoption button2" style="background:url(http://img.xooimage.com/files5/c/o/color-58b80.gif) no-repeat center" title="{L_CHAT_HELP_COLOR}" value="   " /><input type="text" id="chatMsg" class="post inputbox" style="width:70%" maxlength="100" /><input type="button" value="{L_SUBMIT}" onclick="xooitChat.sendMsg();" class="mainoption button2" /> <input type="button" value="{L_SMILIES}" onclick="xooitChat.showSmilies();" class="mainoption button2" />
  </td>
 </tr>
<!--
 <tr class="bg1"><td class="row3"><a href="https://discord.gg/eXPE5H2"><img src="https://lcda.gitlab.io/xooit-files/img/discord.svg" style="height: 1.3em"/> Cliquez ici pour rejoindre le serveur Discord</a></td></tr>
-->
</table>
{CHAT_JS}

